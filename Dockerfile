FROM nvcr.io/nvidia/pytorch:22.06-py3
COPY custom_cuda_models /tmp/custom_cuda_models
WORKDIR /tmp/custom_cuda_models/ops
RUN sh make.sh

FROM nvcr.io/nvidia/tritonserver:22.06-py3
COPY --from=0 /opt/conda/lib/python3.8/site-packages/torch/lib/libtorch_python.so /opt/tritonserver/backends/pytorch/libtorch_python.so
COPY --from=0 /opt/conda/lib/python3.8/site-packages/torch/lib/libshm.so /opt/tritonserver/backends/pytorch/libshm.so
COPY --from=0 /tmp/custom_cuda_models/ops/build/lib.linux-x86_64-3.8/_ext.so /opt/tritonserver/backends/pytorch/_ext.so
ENV LD_LIBRARY_PATH="/opt/tritonserver/backends/pytorch/:$LD_LIBRARY_PATH"
ENV LD_PRELOAD="_ext.so"
# COPY triton-models /models

