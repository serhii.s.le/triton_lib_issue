Run:

```
docker run --rm -it $(docker build -q .)
```

Expect:

```
/bin/bash: symbol lookup error: /opt/tritonserver/backends/pytorch/libtorch_python.so: undefined symbol: PyInstanceMethod_Type
```
